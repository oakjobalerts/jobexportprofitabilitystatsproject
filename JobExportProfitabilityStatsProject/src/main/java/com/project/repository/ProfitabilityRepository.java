package com.project.repository;

import java.util.List;
import java.util.Map;

import com.project.model.CpcReveneStatsModel;
import com.project.model.ExportSourcesModel;
import com.project.model.LpClicksModel;
import com.project.model.ProfitabilityModel;

public interface ProfitabilityRepository {

	List<CpcReveneStatsModel> getAllLandingPageClickStatsDateWiseByYearAndMonth(String year, String month, String exportType);

	List<CpcReveneStatsModel> convertRevenueModelDateWise(List<CpcReveneStatsModel> cpcReveneStatsModels, Map<String, CpcReveneStatsModel> clickMap,
			Map<String, CpcReveneStatsModel> exportProviderMap, List<String> dateList);

	List<CpcReveneStatsModel> getAllClicksFromClickLogTabes(String year, String month, String exportType);

	Map<String, CpcReveneStatsModel> convertStatsModelClicksIntoDatekeyHashMap(List<CpcReveneStatsModel> cpcReveneStatsModels);

	List<CpcReveneStatsModel> getCostAndClicksProvidedByExportSource(String year, String month, String exportType);

	List<ProfitabilityModel> convertCsvStatsModelIntoProfibalityModel(List<CpcReveneStatsModel> cpcReveneStatsModels);

	Map<String, ProfitabilityModel> convertProfibalityModelIntoDaseWiseHashMap(List<ProfitabilityModel> profitabilityModels);

	// String getCsvAsInputStream(Map<String, ProfitabilityModel>
	// profibalityModelMap, String sourceName, List<String> dateLis);

	List<ExportSourcesModel> getExportSourcesList();

	Map<String, CpcReveneStatsModel> getGoogleAdsRevenueDateWise(String year, String month);

	Map<String, CpcReveneStatsModel> getMediaAdsRevenueDateWise(String year, String month);

	Map<String, LpClicksModel> getAllLpClicksFromDataBase(String year, String month);

	Map<String, LpClicksModel> updateLpclickModel_withGoogle_media_revenue(Map<String, CpcReveneStatsModel> googleMap, Map<String, CpcReveneStatsModel> mediaMap,
			Map<String, LpClicksModel> lp_clicl_map, List<String> dateList);

}
