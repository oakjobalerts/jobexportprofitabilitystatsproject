package com.project.repository.impl;

import java.io.BufferedReader;
import java.io.FileReader;
import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.project.model.CpcReveneStatsModel;
import com.project.model.ExportSourcesModel;
import com.project.model.LpClicksModel;
import com.project.model.ProfitabilityModel;
import com.project.repository.ProfitabilityRepository;

@Repository
public class ProfitabilityRepositoryImpl implements ProfitabilityRepository {

	@Autowired
	SessionFactory sessionFactory;

	@Value("${export.sources.json.file.path}")
	String exportSourcesPath;

	String adsRevenueWhitelableForJobexport = "dartalerts";

	@Transactional
	@Override
	public List<CpcReveneStatsModel> getAllLandingPageClickStatsDateWiseByYearAndMonth(String year, String month, String exportType) {

		String query = "select " + "count(distinct email, url,source,IP,provider,cpc,gcpc) as dbUniqueClickCount, "
				+ " count(*) as dbGrossClickCount, "
				+ " ROUND(sum(cpc) * count(DISTINCT email, url,source,IP,provider,cpc,gcpc) / count(*), 2) as uniqueRevenueFromDb, "
				+ " ROUND(sum(cpc), 2) as grossRevenueFromDb,"
				+ " lp_click_type as lpClickType,"
				+ " date(timestamp) as dbDate "
				+ " from tbl_landing_page "
				+ " where month(timestamp) = " + month
				+ " and year(timestamp) = " + year;

		query = query + " and failure_case like '%" + exportType + "%'";

		query = query + " group by lp_click_type, date(timestamp) "
				+ "order by date(timestamp)  desc, source asc";

		Query sqlQuery = getSessionFactory().createSQLQuery(query)
				.setResultTransformer(Transformers.aliasToBean(CpcReveneStatsModel.class));

		@SuppressWarnings("unchecked")
		List<CpcReveneStatsModel> cpcReveneStatsModels = sqlQuery.list();

		return cpcReveneStatsModels;
	}

	@Transactional
	@Override
	public List<CpcReveneStatsModel> getAllClicksFromClickLogTabes(String year, String month, String exportType) {

		String query = "select "
				+ "count(distinct email, url,source,IP,provider,cpc,gcpc) as uniqueCicks,"
				+ "count(email) as grossClicks,"
				+ " ROUND(sum(cpc) * count(DISTINCT email, url,source,IP,provider,cpc,gcpc) / count(*), 2) as uniqueRevenue, "
				+ " ROUND(sum(cpc), 2) as grossRevenue,"
				+ " date(timestamp) as dbDate "
				+ " from tbl_logs_java_testing "
				+ " where month(timestamp) = " + month
				+ " and year(timestamp) = " + year;

		query = query + " and provider = '" + exportType + "'";

		query = query + " group by  date(timestamp) "
				+ "order by date(timestamp)  desc, source asc";

		Query sqlQuery = getSessionFactory().createSQLQuery(query)
				.setResultTransformer(Transformers.aliasToBean(CpcReveneStatsModel.class));

		@SuppressWarnings("unchecked")
		List<CpcReveneStatsModel> cpcReveneStatsModels = sqlQuery.list();

		return cpcReveneStatsModels;

	}

	@Transactional
	@Override
	public List<CpcReveneStatsModel> getCostAndClicksProvidedByExportSource(String year, String month, String exportType) {

		// id, clicks, cost, impressions, jobs_count, export_source, date

		String query = "select " + "clicks as exportSourceClicks,"
				+ " ROUND(cost, 2) as exportSourceCost, "
				+ " date(date) as dbDate "
				+ " from tbl_job_export_cost_stats "
				+ " where month(date) = " + month
				+ " and year(date) = " + year;

		query = query + " and export_source = '" + exportType + "'";

		Query sqlQuery = getSessionFactory().createSQLQuery(query)
				.setResultTransformer(Transformers.aliasToBean(CpcReveneStatsModel.class));

		@SuppressWarnings("unchecked")
		List<CpcReveneStatsModel> cpcReveneStatsModels = sqlQuery.list();

		return cpcReveneStatsModels;

	}

	@Override
	public List<ProfitabilityModel> convertCsvStatsModelIntoProfibalityModel(List<CpcReveneStatsModel> cpcReveneStatsModels) {

		// String date; // Number exportCost; // Number exportClicks; // //
		// grossClicksOak + lpClicks // Number grossClicks; // // exportClicks -
		// grossClicks
		// Number grossClickDiference; // // grossClickDiference / exportClicks
		// // Number grossClickDiffPer; // Number uniqueClicksOak; // Number
		// grossClicksOak;
		// Number lpClicks; // Number flpClicks; // Number uniqueRevenue; //
		// Number grossRevenue; // // uniqueClicksOak - exportClicks // Number
		// clickDifference;
		// // uniqueRevenue - exportCost // Number profit;

		List<ProfitabilityModel> profitabilityModels = new ArrayList<ProfitabilityModel>();

		for (CpcReveneStatsModel cpcReveneStatsModel : cpcReveneStatsModels) {

			ProfitabilityModel profitabilityModel = new ProfitabilityModel();

			profitabilityModel.setDate(cpcReveneStatsModel.getDate());

			// ----------------------------------------------- Update revenue
			// Start
			// FLP Unique revenue
			Number flpUniqueRevenue = cpcReveneStatsModel.getFlpUniqueRevenue();
			if (flpUniqueRevenue == null) {
				flpUniqueRevenue = 0;
			}
			profitabilityModel.setFlpUniqueRevenue(flpUniqueRevenue);
			// FLP Gross revenue
			Number flpGrossRevenue = cpcReveneStatsModel.getFlpGrossRevenue();
			if (flpGrossRevenue == null) {
				flpGrossRevenue = 0;
			}
			profitabilityModel.setFlpGrossRevenue(flpGrossRevenue);

			// SFLP Unique revenue
			Number sflpUniqueRevenue = cpcReveneStatsModel.getSflpUniqueRevenue();
			if (sflpUniqueRevenue == null) {
				sflpUniqueRevenue = 0;
			}
			profitabilityModel.setSflpUniqueRevenue(sflpUniqueRevenue);
			// SFLP Gross revenue
			Number sflpGrossRevenue = cpcReveneStatsModel.getSflpGrossRevenue();
			if (sflpGrossRevenue == null) {
				sflpGrossRevenue = 0;
			}
			profitabilityModel.setSflpGrossRevenue(sflpGrossRevenue);

			// LP Unique revenue
			Number lpUniqueRevenue = cpcReveneStatsModel.getLpUniqueRevenue();
			if (lpUniqueRevenue == null) {
				lpUniqueRevenue = 0;
			}
			profitabilityModel.setLpUniqueRevenue(lpUniqueRevenue);
			// LP Gross revenue
			Number lpGrossRevenue = cpcReveneStatsModel.getLpGrossRevenue();
			if (lpGrossRevenue == null) {
				lpGrossRevenue = 0;
			}
			profitabilityModel.setLpGrossRevenue(lpGrossRevenue);

			// ----------------------------------------------- Update revenue
			// End
			// ----------------------------------------------- Update clicks
			// Start

			// FLP GROSS CLICKS COUNT
			Number flpGrossClicks = cpcReveneStatsModel.getFlpGrossClicks();
			if (flpGrossClicks == null) {
				flpGrossClicks = 0;
			}
			profitabilityModel.setFlpGrossClicks(flpGrossClicks);

			// FLP UNIQUE CLICKS COUNT
			Number flpUniqueClicks = cpcReveneStatsModel.getFlpUniqueClicks();
			if (flpUniqueClicks == null) {
				flpUniqueClicks = 0;
			}
			profitabilityModel.setFlpUniqueClicks(flpUniqueClicks);

			// SFLP GROSS CLICKS COUNT
			Number sflpGrossClicks = cpcReveneStatsModel.getSflpGrossClicks();
			if (sflpGrossClicks == null) {
				sflpGrossClicks = 0;
			}
			profitabilityModel.setSflpGrossClicks(sflpGrossClicks);

			// SFLP UNIQUE CLICKS COUNT
			Number sflpUniqueClicks = cpcReveneStatsModel.getSflpUniqueClicks();
			if (sflpUniqueClicks == null) {
				sflpUniqueClicks = 0;
			}
			profitabilityModel.setSflpUniqueClicks(sflpUniqueClicks);

			// LP GROSS CLICKS COUNT
			Number lpGrossClicks = cpcReveneStatsModel.getLpGrossClicks();
			if (lpGrossClicks == null) {
				lpGrossClicks = 0;
			}
			profitabilityModel.setLpGrossClicks(lpGrossClicks);

			// LP UNIQUE CLICKS COUNT
			Number lpUniqueClicks = cpcReveneStatsModel.getLpUniqueClicks();
			if (lpUniqueClicks == null) {
				lpUniqueClicks = 0;
			}
			profitabilityModel.setLpUniqueClicks(lpUniqueClicks);

			// ----------------------------------------------- Update clicks End

			// Update direct clicks of oak- sflp clicks
			if (cpcReveneStatsModel.getUniqueCicks() == null) {
				cpcReveneStatsModel.setUniqueCicks(0);
			}
			profitabilityModel.setUniqueClicksOak(cpcReveneStatsModel.getUniqueCicks()); // .intValue()
																							// -
																							// sflpUniqueClicks.intValue()

			if (cpcReveneStatsModel.getGrossClicks() == null) {
				cpcReveneStatsModel.setGrossClicks(0);
			}
			profitabilityModel.setGrossClicksOak(cpcReveneStatsModel.getGrossClicks()); // .intValue()
																						// -
																						// sflpGrossClicks.intValue()
			// ------------------------------------

			// Update direct revenue of oak
			if (cpcReveneStatsModel.getUniqueRevenue() == null) {
				cpcReveneStatsModel.setUniqueRevenue(0);
			}
			profitabilityModel.setUniqueRevenue(cpcReveneStatsModel.getUniqueRevenue()); // .floatValue()
																							// -
																							// sflpUniqueRevenue.floatValue()

			if (cpcReveneStatsModel.getGrossRevenue() == null) {
				cpcReveneStatsModel.setGrossRevenue(0);
			}
			profitabilityModel.setGrossRevenue(cpcReveneStatsModel.getGrossRevenue()); // .floatValue()
																						// -
																						// sflpGrossRevenue.floatValue()
			// ------------------------------------

			// JOB EXPORT CLICKS PROVIDED BY PROVIDERS
			Number exportClicks = cpcReveneStatsModel.getExportSourceClicks();
			if (exportClicks == null) {
				exportClicks = 0;
			}
			profitabilityModel.setExportClicks(exportClicks);

			// JOBS EXPORT COST PROVIDED BY PROVIDERS
			Number exportCost = cpcReveneStatsModel.getExportSourceCost();
			if (exportCost == null) {
				exportCost = 0;
			}
			profitabilityModel.setExportCost(exportCost);

			// profitabilityModel.setGrossClicks(lpClick.intValue() +
			// grossClicks.intValue() + flpClicks.intValue());
			// //
			// profitabilityModel.setGrossClickDiffPer(exportClicks.intValue() -
			// grossClicks.intValue());
			// // profitabilityModel.setGrossClickDiffPer(grossClickDiffPer);
			// profitabilityModel.setLpClicks(cpcReveneStatsModel.getLpUniqueClickCount());
			// profitabilityModel.setClickDifference(uniqueClicks.intValue() -
			// exportClicks.intValue());
			// profitabilityModel.setProfit((float)
			// (Math.round((uniqueRevenue.floatValue() - exportCost.floatValue()
			// + flpRevenue.floatValue()) * 1000000.0) / 1000000.0));
			// profitabilityModel.setGrossClickDiference(exportClicks.intValue()
			// - profitabilityModel.getGrossClicks().intValue());

			profitabilityModels.add(profitabilityModel);

		}

		return profitabilityModels;
	}

	@Override
	public Map<String, CpcReveneStatsModel> convertStatsModelClicksIntoDatekeyHashMap(List<CpcReveneStatsModel> cpcReveneStatsModels) {

		Map<String, CpcReveneStatsModel> clickMap = new HashMap<String, CpcReveneStatsModel>();

		for (CpcReveneStatsModel cpcReveneStatsModel : cpcReveneStatsModels) {

			clickMap.put(cpcReveneStatsModel.getDbDate() + "", cpcReveneStatsModel);

		}

		return clickMap;
	}

	@Override
	public List<CpcReveneStatsModel> convertRevenueModelDateWise(List<CpcReveneStatsModel> cpcReveneStatsModels, Map<String, CpcReveneStatsModel> clickMap,
			Map<String, CpcReveneStatsModel> exportProviderMap, List<String> dateList) {

		List<CpcReveneStatsModel> cpcReveneStatsModelsUpdated = new ArrayList<CpcReveneStatsModel>();

		for (String date : dateList) {

			CpcReveneStatsModel cpcModel = new CpcReveneStatsModel();

			cpcModel.setLpUniqueRevenue(0);
			cpcModel.setLpGrossRevenue(0);

			cpcModel.setFlpUniqueRevenue(0);
			cpcModel.setFlpGrossRevenue(0);

			cpcModel.setSflpUniqueRevenue(0);
			cpcModel.setSflpGrossRevenue(0);

			cpcModel.setDbUniqueClickCount(0);
			cpcModel.setDbGrossClickCount(0);

			cpcModel.setSflpGrossClicks(0);
			cpcModel.setFlpGrossClicks(0);
			cpcModel.setLpGrossClicks(0);

			cpcModel.setSflpUniqueClicks(0);
			cpcModel.setFlpUniqueClicks(0);
			cpcModel.setLpUniqueClicks(0);

			boolean dateFound = false;

			for (CpcReveneStatsModel cpcReveneStatsModel : cpcReveneStatsModels) {

				if (date.equals(cpcReveneStatsModel.getDbDate() + "")) {

					cpcModel.setDate(cpcReveneStatsModel.getDbDate() + "");

					if (cpcReveneStatsModel.getLpClickType().equalsIgnoreCase("LP")) {

						cpcModel.setLpUniqueClicks(cpcReveneStatsModel.getDbUniqueClickCount());
						cpcModel.setLpGrossClicks(cpcReveneStatsModel.getDbGrossClickCount());

						cpcModel.setLpUniqueRevenue(cpcModel.getLpUniqueRevenue().doubleValue() + cpcReveneStatsModel.getUniqueRevenueFromDb().doubleValue());
						cpcModel.setLpGrossRevenue(cpcModel.getLpGrossRevenue().doubleValue() + cpcReveneStatsModel.getGrossRevenueFromDb().doubleValue());

					} else if (cpcReveneStatsModel.getLpClickType().equalsIgnoreCase("SFLP")) {

						cpcModel.setSflpUniqueClicks(cpcReveneStatsModel.getDbUniqueClickCount());
						cpcModel.setSflpGrossClicks(cpcReveneStatsModel.getDbGrossClickCount());

						cpcModel.setSflpUniqueRevenue(cpcModel.getSflpUniqueRevenue().doubleValue() + cpcReveneStatsModel.getUniqueRevenueFromDb().doubleValue());
						cpcModel.setSflpGrossRevenue(cpcModel.getSflpGrossRevenue().doubleValue() + cpcReveneStatsModel.getGrossRevenueFromDb().doubleValue());

					} else if (cpcReveneStatsModel.getLpClickType().equalsIgnoreCase("FLP")) {

						cpcModel.setFlpUniqueClicks(cpcReveneStatsModel.getDbUniqueClickCount());
						cpcModel.setFlpGrossClicks(cpcReveneStatsModel.getDbGrossClickCount());

						cpcModel.setFlpUniqueRevenue(cpcModel.getFlpUniqueRevenue().doubleValue() + cpcReveneStatsModel.getUniqueRevenueFromDb().doubleValue());
						cpcModel.setFlpGrossRevenue(cpcModel.getFlpGrossRevenue().doubleValue() + cpcReveneStatsModel.getGrossRevenueFromDb().doubleValue());
					}

					dateFound = true;
				}
			}

			// If landing page model date not found in tbl_landing_page set date
			if (!dateFound) {

				cpcModel.setDate(date);
			}

			CpcReveneStatsModel actualClickModel = clickMap.get(date);
			if (actualClickModel != null) {
				cpcModel.setUniqueCicks(actualClickModel.getUniqueCicks());
				cpcModel.setGrossClicks(actualClickModel.getGrossClicks());
				cpcModel.setUniqueRevenue(actualClickModel.getUniqueRevenue());
				cpcModel.setGrossRevenue(actualClickModel.getGrossRevenue());
			} else {
				cpcModel.setUniqueCicks(0);
				cpcModel.setGrossClicks(0);
				cpcModel.setUniqueRevenue(0);
				cpcModel.setGrossRevenue(0);
			}

			CpcReveneStatsModel providerStatsClicks = exportProviderMap.get(date);
			if (providerStatsClicks != null) {
				cpcModel.setExportSourceClicks(providerStatsClicks.getExportSourceClicks());
				cpcModel.setExportSourceCost(providerStatsClicks.getExportSourceCost());
			} else {
				cpcModel.setExportSourceClicks(0);
				cpcModel.setExportSourceCost(0);
			}

			cpcReveneStatsModelsUpdated.add(cpcModel);

		}

		return cpcReveneStatsModelsUpdated;
	}

	@Override
	public Map<String, ProfitabilityModel> convertProfibalityModelIntoDaseWiseHashMap(List<ProfitabilityModel> profitabilityModels) {

		Map<String, ProfitabilityModel> profibalityModelMap = new HashMap<String, ProfitabilityModel>();

		for (ProfitabilityModel profitabilityModel : profitabilityModels) {

			profibalityModelMap.put(profitabilityModel.getDate(), profitabilityModel);
		}

		return profibalityModelMap;
	}

	// @Override
	// public String getCsvAsInputStream(Map<String, ProfitabilityModel>
	// profibalityModelMap, String sourceName, List<String> dateLis) {
	//
	// String firstRow = "Date," + sourceName + " Cost," + sourceName +
	// " Clicks,Gross Clicks,Gross Click Difference,Unique Job Clicks(OJA),Gross Job Clicks(OJA),LP Clicks,"
	// +
	// "FLP Clicks,FLP Revenue,Unique Revenue,Gross Revenue,Click Difference,Profit"
	// + "\n";
	//
	// String statsBody = "";
	//
	// float totalExportCost = 0;
	// int totalExportClicks = 0;
	// int totalGrossClicks = 0;
	// int totalGrossClickDifference = 0;
	// int totalUniqueClicksOak = 0;
	// int totalGrossClicksOak = 0;
	// int totalLpClicks = 0;
	// int totalFlpClicks = 0;
	// float totalFlpRevenue = 0;
	// float totalUniqueRevenue = 0;
	// float totalGrossRevenue = 0;
	// int totalClickDifference = 0;
	// float totalProfit = 0;
	//
	// for (String date : dateLis) {
	//
	// ProfitabilityModel profitabilityModel = profibalityModelMap.get(date);
	//
	// if (profitabilityModel == null) {
	// continue;
	// }
	//
	// statsBody = statsBody + ""
	// + profitabilityModel.getDate() + ","
	// + profitabilityModel.getExportCost() + ","
	// + profitabilityModel.getExportClicks() + ","
	// + profitabilityModel.getGrossClicks() + ","
	// + profitabilityModel.getGrossClickDiference() + ","
	// + profitabilityModel.getUniqueClicksOak() + ","
	// + profitabilityModel.getGrossClicksOak() + ","
	// + profitabilityModel.getLpClicks() + ","
	// + profitabilityModel.getFlpClicks() + ","
	// + profitabilityModel.getFlpRevenue() + ","
	// + profitabilityModel.getUniqueRevenue() + ","
	// + profitabilityModel.getGrossRevenue() + ","
	// + profitabilityModel.getClickDifference() + ","
	// + profitabilityModel.getProfit() + "\n";
	//
	// totalExportCost = totalExportCost +
	// profitabilityModel.getExportCost().floatValue();
	// totalExportClicks = totalExportClicks +
	// profitabilityModel.getExportClicks().intValue();
	// totalGrossClicks = totalGrossClicks +
	// profitabilityModel.getGrossClicks().intValue();
	// totalGrossClickDifference = totalGrossClickDifference +
	// profitabilityModel.getGrossClickDiference().intValue();
	// totalUniqueClicksOak = totalUniqueClicksOak +
	// profitabilityModel.getUniqueClicksOak().intValue();
	// totalGrossClicksOak = totalGrossClicksOak +
	// profitabilityModel.getGrossClicksOak().intValue();
	// totalLpClicks = totalLpClicks +
	// profitabilityModel.getLpClicks().intValue();
	// totalFlpClicks = totalFlpClicks +
	// profitabilityModel.getFlpClicks().intValue();
	// totalFlpRevenue = totalFlpRevenue +
	// profitabilityModel.getFlpRevenue().floatValue();
	// totalUniqueRevenue = totalUniqueRevenue +
	// profitabilityModel.getUniqueRevenue().floatValue();
	// totalGrossRevenue = totalGrossRevenue +
	// profitabilityModel.getGrossRevenue().floatValue();
	// totalClickDifference = totalClickDifference +
	// profitabilityModel.getClickDifference().intValue();
	// totalProfit = totalProfit + profitabilityModel.getProfit().floatValue();
	//
	// }
	//
	// firstRow = firstRow + ""
	// + "Total" + ","
	// + totalExportCost + ","
	// + totalExportClicks + ","
	// + totalGrossClicks + ","
	// + totalGrossClickDifference + ","
	// + totalUniqueClicksOak + ","
	// + totalGrossClicksOak + ","
	// + totalLpClicks + ","
	// + totalFlpClicks + ","
	// + totalFlpRevenue + ","
	// + totalUniqueRevenue + ","
	// + totalGrossRevenue + ","
	// + totalClickDifference + ","
	// + totalProfit + "\n";
	//
	// firstRow = firstRow + statsBody;
	//
	// return firstRow;
	// }

	@Override
	public List<ExportSourcesModel> getExportSourcesList() {

		Type type = new TypeToken<List<ExportSourcesModel>>() {
		}.getType();

		List<ExportSourcesModel> exportModelList = new ArrayList<ExportSourcesModel>();

		try {
			BufferedReader bufferedReader = new BufferedReader(new FileReader(exportSourcesPath));
			exportModelList = new Gson().fromJson(bufferedReader, type);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return exportModelList;
	}

	public Session getSessionFactory() {
		return sessionFactory.getCurrentSession();
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Transactional
	@Override
	public Map<String, CpcReveneStatsModel> getGoogleAdsRevenueDateWise(String year, String month) {

		// TODO Auto-generated method stub

		String query = "select sum(estimated_earning) as googleAdsRevenue, date(date) as dbDate"
				+ " from tbl_google_adsense_revenue "
				+ " where  whitelabel='" + adsRevenueWhitelableForJobexport + "' and month(date) = " + month
				+ " and year(date) = " + year;

		query = query + " group by date(date) "
				+ "order by date(date)  asc";

		System.out.println(query);

		Query sqlQuery = getSessionFactory().createSQLQuery(query)
				.setResultTransformer(Transformers.aliasToBean(CpcReveneStatsModel.class));

		@SuppressWarnings("unchecked")
		List<CpcReveneStatsModel> profitabilityModelist = sqlQuery.list();

		Map<String, CpcReveneStatsModel> googleAdsRevenueMap = new HashMap<String, CpcReveneStatsModel>();

		for (CpcReveneStatsModel cpcReveneStatsModel : profitabilityModelist) {

			googleAdsRevenueMap.put(cpcReveneStatsModel.getDbDate() + "", cpcReveneStatsModel);

		}

		return googleAdsRevenueMap;

	}

	@Transactional
	@Override
	public Map<String, CpcReveneStatsModel> getMediaAdsRevenueDateWise(String year, String month) {
		// TODO Auto-generated method stub

		String query = "select sum(estimated_revenue) as mediaAdsRevenue, date(date) as dbDate"
				+ " from tbl_media_net_revenue "
				+ " where  whitelabel='" + adsRevenueWhitelableForJobexport + "'  and landing_page_type='landing_page_3' and month(date) = " + month
				+ " and year(date) = " + year;

		query = query + " group by date(date) "
				+ "order by date(date)  asc";

		System.out.println(query);

		Query sqlQuery = getSessionFactory().createSQLQuery(query)
				.setResultTransformer(Transformers.aliasToBean(CpcReveneStatsModel.class));

		@SuppressWarnings("unchecked")
		List<CpcReveneStatsModel> profitabilityModelist = sqlQuery.list();

		Map<String, CpcReveneStatsModel> mediaAdsRevenueMap = new HashMap<String, CpcReveneStatsModel>();

		for (CpcReveneStatsModel cpcReveneStatsModel : profitabilityModelist) {

			mediaAdsRevenueMap.put(cpcReveneStatsModel.getDbDate() + "", cpcReveneStatsModel);

		}

		return mediaAdsRevenueMap;

	}

	@Transactional
	@Override
	public Map<String, LpClicksModel> getAllLpClicksFromDataBase(String year, String month) {
		// TODO Auto-generated method stub
		
		
		
				

		// TODO Auto-generated method stub

		String query = "select count(distinct email, url,source,IP,provider,cpc,gcpc) as lp_count,date(timestamp) as dbDate from tbl_landing_page where main_provider like '%jobexport%' and date(timestamp)"
				+ "and lp_click_type='LP' and month(timestamp) = " + month
				+ " and year(timestamp) = " + year;

		query = query + " group by date(timestamp) "
				+ "order by date(timestamp) asc";

		Query sqlQuery = getSessionFactory().createSQLQuery(query)
				.setResultTransformer(Transformers.aliasToBean(LpClicksModel.class));

		@SuppressWarnings("unchecked")
		List<LpClicksModel> lp_clicks_list = sqlQuery.list();

		Map<String, LpClicksModel> lp_clicks_map = new HashMap<String, LpClicksModel>();

		for (LpClicksModel lpclickModel : lp_clicks_list) {

			lp_clicks_map.put(lpclickModel.getDbDate() + "", lpclickModel);

		}

		return lp_clicks_map;

	}

	@Override
	public Map<String, LpClicksModel> updateLpclickModel_withGoogle_media_revenue(Map<String, CpcReveneStatsModel> googleMap, Map<String, CpcReveneStatsModel> mediaMap,
			Map<String, LpClicksModel> lp_clicl_map, List<String> dateList) {
		// TODO Auto-generated method stub

		for (String date : dateList) {

			if (lp_clicl_map.get(date) != null) {

				LpClicksModel lpModel = lp_clicl_map.get(date);
				CpcReveneStatsModel googleModel = googleMap.get(date);
				CpcReveneStatsModel mediaModel = mediaMap.get(date);
				if (googleModel != null && googleModel.getGoogleAdsRevenue() != null)
					lpModel.setGoogleUpdatedAdsRevenue(googleModel.getGoogleAdsRevenue().floatValue() / lpModel.getLp_count().floatValue());
				if (mediaModel != null && mediaModel.getMediaAdsRevenue() != null)
					lpModel.setMediaUpdatedAdsRevenue(mediaModel.getMediaAdsRevenue().floatValue() / lpModel.getLp_count().floatValue());

				lp_clicl_map.put(date, lpModel);
			}

		}
		return lp_clicl_map;
	}

}
