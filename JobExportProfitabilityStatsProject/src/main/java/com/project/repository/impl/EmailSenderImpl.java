package com.project.repository.impl;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.stereotype.Service;

import com.project.model.EmailReceiver;
import com.project.model.LpClicksModel;
import com.project.model.ProfitabilityModel;
import com.project.repository.EmailSender;
import com.project.utility.DateUtility;

@Service
public class EmailSenderImpl implements EmailSender {

	@Value("${mail.smtp.user}")
	String user;

	@Value("${mail.smtp.password}")
	String pass;

	@Value("${mail.smtp.host}")
	String mailSmtpHost;

	@Value("${mail.transport.protocol}")
	String mailTransportProtocol;

	@Value("${mail.smtp.port}")
	Integer mailSmtpPort;

	@Value("${mail.smtp.auth}")
	String mailSmtpAuth;

	@Value("${mail.smtp.socketFactory.port}")
	Integer mailSmtpSocketFactoryPort;

	@Value("${mail.smtp.socketFactory.class}")
	String mailSmtpSocketFactoryClass;

	@Autowired
	DateUtility dateUtility;

	@Override
	public String sendSmtpEmail(String subject, String body, EmailReceiver emailReceiver, String csvData) {

		Properties props = System.getProperties();

		props.put("mail.smtp.host", mailSmtpHost);
		props.put("mail.transport.protocol", mailTransportProtocol);
		props.put("mail.smtp.port", mailSmtpPort);
		props.put("mail.smtp.auth", mailSmtpAuth);
		props.put("mail.smtp.password", pass);
		props.put("mail.smtp.socketFactory.port", mailSmtpSocketFactoryPort);
		props.put("mail.smtp.socketFactory.class", mailSmtpSocketFactoryClass);

		Authenticator auth = new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(user, pass);
			}
		};

		Session session = Session.getInstance(props, auth);

		try {
			MimeMessage msg = new MimeMessage(session);

			for (String cc : emailReceiver.getCcList()) {
				msg.addRecipients(Message.RecipientType.CC, InternetAddress.parse(cc));
			}

			for (String bcc : emailReceiver.getBccList()) {
				msg.addRecipients(Message.RecipientType.BCC, InternetAddress.parse(bcc));
			}

			msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
			msg.addHeader("format", "flowed");
			msg.addHeader("Content-Transfer-Encoding", "8bit");
			msg.setSubject(subject, "UTF-8");
			msg.setContent(body, "text/html");

			msg.setFrom(new InternetAddress(user, "FBG Reports"));
			msg.setReplyTo(InternetAddress.parse(user, false));

			msg.setSentDate(new Date());

			MimeBodyPart messageBodyPart = new MimeBodyPart();
			DataSource ds = new ByteArrayDataSource(csvData, "application/csv");
			messageBodyPart.setDataHandler(new DataHandler(ds));
			messageBodyPart.setFileName(subject.replace(" ", "_") + ".csv");

			MimeBodyPart textBodyPart = new MimeBodyPart();
			// textBodyPart.setText(body);
			textBodyPart.setContent(body, "text/html");

			MimeMultipart mimeMultipart = new MimeMultipart();
			mimeMultipart.addBodyPart(messageBodyPart);
			mimeMultipart.addBodyPart(textBodyPart);

			msg.setContent(mimeMultipart);

			msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailReceiver.getTo(), false));
			Transport.send(msg);

		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("Email Sent Successfully!! for : " + subject);
		return "Email Sent Successfully!!";
	}

	@Override
	public List<String> createEmailBody(Map<String, ProfitabilityModel> profibalityModelMap, String sourceName, List<String> dateLis, Map<String, LpClicksModel> lp_click_map) {

		List<String> emailAndCsvBodyList = new ArrayList<String>();

		DecimalFormat decimal_Format = new DecimalFormat("###.##");
		String format = decimal_Format.format(123.456);

		Collections.reverse(dateLis);

		String csvStringFirstRow = "Date,"
				+ "" + sourceName + " Cost,"
				+ sourceName + " Clicks,"
				+ "FBG GC (GC + LP),"
				+ "FBG UC (UC + LP),"
				+ "FBG UC + SFLP + FLP,"
				+ "FBG DUC,"
				+ "SFLP UC,"
				+ "FLP UC,"
				+ "Paid Clicks (DUC + U SFLP + U FLP),"
				+ "Gross Click Difference ( " + sourceName + " Clicks - FBG GC),"
				+ "LP GC,"
				+ "DCU Revenue,"
				+ "SFLP UC Revenue,"
				+ "FLP U Revenue,"
				+ "Click Difference (" + sourceName + " clicks - FBG UC),"
				+ "Google Revenue"
				+ "Media.net Revenue"
				+ "Profit/Loss (Paid Clicks - " + sourceName + " clicks )"
				+ "\n";

		String csvStatsBody = "";
		String body = "";
		body = body + "<div>"
				+ "<table cellspacing='0' cellpadding='12' border='0' >"
				+ "<thead>"
				+ "<tr>"
				+ "<th style='border-bottom: 2px solid #C35D5D;'>Date</th>"
				+ "<th style='border-bottom: 2px solid #C35D5D;'>" + sourceName + " Cost</th>"
				+ "<th style='border-bottom: 2px solid #C35D5D;'>" + sourceName + " Clicks</th>"
				+ "<th style='border-bottom: 2px solid #C35D5D;'>FBG GC (GC + LP)</th>"
				+ "<th style='border-bottom: 2px solid #C35D5D;'>FBG UC (UC + LP)</th>"
				+ "<th style='border-bottom: 2px solid #C35D5D;'>FBG UC + SFLP + FLP</th>"
				+ "<th style='border-bottom: 2px solid #C35D5D;'>FBG DUC</th>"
				+ "<th style='border-bottom: 2px solid #C35D5D;'>SFLP UC</th>"
				+ "<th style='border-bottom: 2px solid #C35D5D;'>FLP UC</th>"
				+ "<th style='border-bottom: 2px solid #C35D5D;'>Paid Clicks (DUC + U SFLP + U FLP)</th>"
				+ "<th style='border-bottom: 2px solid #C35D5D;'>Gross Click Difference ( " + sourceName + " Clicks - FBG GC)</th>"
				+ "<th style='border-bottom: 2px solid #C35D5D;'>LP GC</th>"
				+ "<th style='border-bottom: 2px solid #C35D5D;'>DCU Revenue</th>"
				+ "<th style='border-bottom: 2px solid #C35D5D;'>SFLP UC Revenue</th>"
				+ "<th style='border-bottom: 2px solid #C35D5D;'>FLP U Revenue</th>"
				+ "<th style='border-bottom: 2px solid #C35D5D;'>Click Difference (" + sourceName + " clicks - FBG UC)</th>"
				+ "<th style='border-bottom: 2px solid #C35D5D;'>Google Revenue</th>"
				+ "<th style='border-bottom: 2px solid #C35D5D;'>Media.net Revenue</th>"
				+ "<th style='border-bottom: 2px solid #C35D5D;'>Profit/Loss (Paid Clicks - " + sourceName + " clicks)</th>"
				+ "</tr>"
				+ "</thead>"
				+ "<tbody style='vertical-align: text-top;text-align: center;'>";

		float exportSourceCostTotal = 0;
		int exportSourceClicksTotal = 0;
		int fbgGrossClicksTotal = 0;
		int fbgUniqueClicksTotal = 0;
		int paidClicksTotal = 0;
		int directClicksTotal = 0;
		float sflpUniqueClicksTotal = 0;
		int flpUniqueClicksTotal = 0;
		int grossClickDifferenceTotal = 0;
		int lpGrossClicksTotal = 0;
		float directClickUniqueRevenueTotal = 0;
		float sflpUniqueClicksRevenueTotal = 0;
		float flpUniqueClicksRevenueTotal = 0;
		int uniqueClickDifferenceTotal = 0;

		float googleAdsRevenueTotal = 0;
		float medianetRevenueTotal = 0;

		float profitTotal = 0;

		String statsBody = "";

		for (String date : dateLis) {

			ProfitabilityModel profitabilityModel = profibalityModelMap.get(date);

			if (profitabilityModel == null) {
				continue;
			}

			if (date.equalsIgnoreCase(dateUtility.getTodayDate())) {
				continue;
			}

			statsBody = statsBody + "<tr style='border-collapse:collapse'>"
					+ "<td style='border-bottom: 2px solid #0e5029;'>" + profitabilityModel.getDate() + "</td>";
			csvStatsBody = csvStatsBody + profitabilityModel.getDate() + ",";

			float exportSourceCost = profitabilityModel.getExportCost().floatValue();
			csvStatsBody = csvStatsBody + exportSourceCost + ",";
			exportSourceCostTotal = Float.valueOf(decimal_Format.format(exportSourceCostTotal + exportSourceCost));
			statsBody = statsBody + "<td style='border-bottom: 2px solid #0e5029;" + addNegativeValueColor(exportSourceCost) + "'>" + "$" + exportSourceCost + "</td>";

			int exportSourceClicks = profitabilityModel.getExportClicks().intValue();
			csvStatsBody = csvStatsBody + exportSourceClicks + ",";
			exportSourceClicksTotal = exportSourceClicksTotal + exportSourceClicks;
			statsBody = statsBody + "<td style='border-bottom: 2px solid #0e5029;" + addNegativeValueColor(exportSourceClicks) + "'>" + exportSourceClicks + "</td>";

			// fbgGrossClicks = Gross clicks + LP clicks - SFLP - FLP clicks (as
			// sflp and flp clicks are already counted by LP clicks)
			int fbgGrossClicks = profitabilityModel.getGrossClicksOak().intValue() + profitabilityModel.getLpGrossClicks().intValue()
					- profitabilityModel.getSflpGrossClicks().intValue() - profitabilityModel.getFlpGrossClicks().intValue();
			csvStatsBody = csvStatsBody + fbgGrossClicks + ",";
			fbgGrossClicksTotal = fbgGrossClicksTotal + fbgGrossClicks;
			statsBody = statsBody + "<td style='border-bottom: 2px solid #0e5029;" + addNegativeValueColor(fbgGrossClicks) + "'>" + (fbgGrossClicks) + "</td>";

			// lpUniqueClicks = Unique clicks + LP clicks - SFLP - FLP clicks
			// (as sflp and flp clicks are already counted by LP clicks)
			int fbgUniqueClicks = profitabilityModel.getUniqueClicksOak().intValue() + profitabilityModel.getLpUniqueClicks().intValue()
					- profitabilityModel.getSflpUniqueClicks().intValue() - profitabilityModel.getFlpUniqueClicks().intValue();
			csvStatsBody = csvStatsBody + fbgUniqueClicks + ",";
			fbgUniqueClicksTotal = fbgUniqueClicksTotal + fbgUniqueClicks;
			statsBody = statsBody + "<td style='border-bottom: 2px solid #0e5029;" + addNegativeValueColor(fbgUniqueClicks) + "'>" + (fbgUniqueClicks) + "</td>";

			// FBG unique clicks already have sflp + flp clicks as with the new
			// change in tracking
			int paidClicks = profitabilityModel.getUniqueClicksOak().intValue();
			csvStatsBody = csvStatsBody + paidClicks + ",";
			paidClicksTotal = paidClicksTotal + paidClicks;
			statsBody = statsBody + "<td style='border-bottom: 2px solid #0e5029;" + addNegativeValueColor(paidClicks) + "'>" + (paidClicks) + "</td>";

			// Direct clicks need to subtract sflp and flp clicks
			int directClicks = profitabilityModel.getUniqueClicksOak().intValue() - profitabilityModel.getSflpUniqueClicks().intValue() - profitabilityModel.getFlpUniqueClicks().intValue();
			csvStatsBody = csvStatsBody + directClicks + ",";
			directClicksTotal = directClicksTotal + directClicks;
			statsBody = statsBody + "<td style='border-bottom: 2px solid #0e5029;" + addNegativeValueColor(directClicks) + "'>" + directClicks + "</td>"; // FBG
																																							// Direct
																																							// Unique
																																							// Clicks

			// SFLP Unique Clicks
			int sflpUniqueClicks = profitabilityModel.getSflpUniqueClicks().intValue();
			csvStatsBody = csvStatsBody + sflpUniqueClicks + ",";
			sflpUniqueClicksTotal = sflpUniqueClicksTotal + sflpUniqueClicks;
			statsBody = statsBody + "<td style='border-bottom: 2px solid #0e5029;" + addNegativeValueColor(sflpUniqueClicks) + "'>" + sflpUniqueClicks + "</td>";

			// FLP Unique Clicks
			int flpUniqueClicks = profitabilityModel.getFlpUniqueClicks().intValue();
			csvStatsBody = csvStatsBody + flpUniqueClicks + ",";
			flpUniqueClicksTotal = flpUniqueClicksTotal + flpUniqueClicks;
			statsBody = statsBody + "<td style='border-bottom: 2px solid #0e5029;" + addNegativeValueColor(flpUniqueClicks) + "'>" + flpUniqueClicks + "</td>";

			// Paid Clicks (Direct Unique Clicks + Unique SFLP + Unique FLP)
			csvStatsBody = csvStatsBody + paidClicks + ",";
			statsBody = statsBody + "<td style='border-bottom: 2px solid #0e5029;" + addNegativeValueColor(paidClicks) + "'>" + paidClicks + "</td>";

			// Gross Click Difference ( sourceName Clicks - fbgGrossClicks )
			int grossClickDifference = profitabilityModel.getExportClicks().intValue() - fbgGrossClicks;
			csvStatsBody = csvStatsBody + grossClickDifference + ",";
			grossClickDifferenceTotal = grossClickDifferenceTotal + grossClickDifference;
			statsBody = statsBody + "<td style='border-bottom: 2px solid #0e5029;" + addNegativeValueColor(grossClickDifference) + "'>" + grossClickDifference + "</td>";

			// LP Gross Clicks
			int lpGrossClicks = profitabilityModel.getLpGrossClicks().intValue();
			csvStatsBody = csvStatsBody + lpGrossClicks + ",";
			lpGrossClicksTotal = lpGrossClicksTotal + lpGrossClicks;
			statsBody = statsBody + "<td style='border-bottom: 2px solid #0e5029;" + addNegativeValueColor(lpGrossClicks) + "'>" + lpGrossClicks + "</td>";

			// Direct Click Unique Revenue (DC - SFLP - FLP)
			float directClickUniqueRevenue = profitabilityModel.getUniqueRevenue().floatValue()
					- profitabilityModel.getSflpUniqueRevenue().floatValue() - profitabilityModel.getFlpUniqueRevenue().floatValue();
			csvStatsBody = csvStatsBody + directClickUniqueRevenue + ",";
			directClickUniqueRevenueTotal = directClickUniqueRevenueTotal + directClickUniqueRevenue;
			statsBody = statsBody + "<td style='border-bottom: 2px solid #0e5029;" + addNegativeValueColor(directClickUniqueRevenue) + "'>" + "$" + directClickUniqueRevenue + "</td>";

			// SFLP Click Unique Revenue
			float sflpUniqueClicksRevenue = profitabilityModel.getSflpUniqueRevenue().floatValue();
			csvStatsBody = csvStatsBody + sflpUniqueClicksRevenue + ",";
			sflpUniqueClicksRevenueTotal = sflpUniqueClicksRevenueTotal + sflpUniqueClicksRevenue;
			statsBody = statsBody + "<td style='border-bottom: 2px solid #0e5029;" + addNegativeValueColor(sflpUniqueClicksRevenue) + "'>" + "$" + sflpUniqueClicksRevenue + "</td>";

			// FLP Unique Revenue
			float flpUniqueClickRevenue = profitabilityModel.getFlpUniqueRevenue().floatValue();
			csvStatsBody = csvStatsBody + flpUniqueClickRevenue + ",";
			flpUniqueClicksRevenueTotal = flpUniqueClicksRevenueTotal + flpUniqueClickRevenue;
			statsBody = statsBody + "<td style='border-bottom: 2px solid #0e5029;" + addNegativeValueColor(flpUniqueClickRevenue) + "'>" + "$" + flpUniqueClickRevenue + "</td>";

			// Unique Click Difference (" + sourceName + " clicks - FBG UC )
			int uniqueClickDifference = profitabilityModel.getExportClicks().intValue()
					- (profitabilityModel.getUniqueClicksOak().intValue() + profitabilityModel.getLpUniqueClicks().intValue() - profitabilityModel.getFlpUniqueClicks().intValue() - profitabilityModel
							.getSflpUniqueClicks().intValue());
			csvStatsBody = csvStatsBody + uniqueClickDifference + ",";
			uniqueClickDifferenceTotal = uniqueClickDifferenceTotal + uniqueClickDifference;
			statsBody = statsBody + "<td style='border-bottom: 2px solid #0e5029;" + addNegativeValueColor(uniqueClickDifference) + "'>" + uniqueClickDifference + "</td>";

			LpClicksModel lp_click_model = lp_click_map.get(date);
			float googleAds_revnue = 0;
			float mediaAds_revnue = 0;
			if (lp_click_model != null) {

				googleAds_revnue = profitabilityModel.getLpUniqueClicks().intValue() * lp_click_model.getGoogleUpdatedAdsRevenue();
				mediaAds_revnue = profitabilityModel.getLpUniqueClicks().intValue() * lp_click_model.getMediaUpdatedAdsRevenue();
			}

			csvStatsBody = csvStatsBody + googleAds_revnue + ",";
			googleAdsRevenueTotal = googleAdsRevenueTotal + googleAds_revnue;
			statsBody = statsBody + "<td style='border-bottom: 2px solid #0e5029;" + addNegativeValueColor(Float.valueOf(decimal_Format.format(googleAds_revnue))) + "'>" + "$"
					+ decimal_Format.format(googleAds_revnue) + "</td>";

			csvStatsBody = csvStatsBody + mediaAds_revnue + ",";
			medianetRevenueTotal = medianetRevenueTotal + mediaAds_revnue;
			statsBody = statsBody + "<td style='border-bottom: 2px solid #0e5029;" + addNegativeValueColor(Float.valueOf(decimal_Format.format(mediaAds_revnue))) + "'>" + "$"
					+ decimal_Format.format(mediaAds_revnue) + "</td>";

			// Profit/Loss -- unique clicks(direct, sflp, flp) - export cost
			float profit = profitabilityModel.getUniqueRevenue().floatValue() - profitabilityModel.getExportCost().floatValue() + googleAds_revnue + mediaAds_revnue;
			csvStatsBody = csvStatsBody + profit + ",";
			profitTotal = profitTotal + profit;
			statsBody = statsBody + "<td style='border-bottom: 2px solid #0e5029;" + addNegativeValueColor(Float.valueOf(decimal_Format.format(profit))) + "'>" + "$"
					+ Float.valueOf(decimal_Format.format(profit)) + "</td>";

			csvStatsBody = csvStatsBody + "\n";
			statsBody = statsBody + " </tr>";
		}

		body = body + "<tr style='border-collapse:collapse'>"
				+ "<td style='border-bottom: 2px solid #0e5029; font-weight: bold;'>" + "Total" + "</td>"
				+ "<td style='border-bottom: 2px solid #0e5029; font-weight: bold;" + addNegativeValueColor(Float.valueOf(decimal_Format.format(exportSourceCostTotal))) + "'>" + "$" + Float.valueOf(decimal_Format.format(exportSourceCostTotal)) + "</td>"
				+ "<td style='border-bottom: 2px solid #0e5029; font-weight: bold;" + addNegativeValueColor(exportSourceClicksTotal) + "'>" + exportSourceClicksTotal + "</td>"
				+ "<td style='border-bottom: 2px solid #0e5029; font-weight: bold;" + addNegativeValueColor(fbgGrossClicksTotal) + "'>" + fbgGrossClicksTotal + "</td>"
				+ "<td style='border-bottom: 2px solid #0e5029; font-weight: bold;" + addNegativeValueColor(fbgUniqueClicksTotal) + "'>" + fbgUniqueClicksTotal + "</td>"
				+ "<td style='border-bottom: 2px solid #0e5029; font-weight: bold;" + addNegativeValueColor(paidClicksTotal) + "'>" + paidClicksTotal + "</td>"
				+ "<td style='border-bottom: 2px solid #0e5029; font-weight: bold;" + addNegativeValueColor(directClicksTotal) + "'>" + directClicksTotal + "</td>"
				+ "<td style='border-bottom: 2px solid #0e5029; font-weight: bold;" + addNegativeValueColor(sflpUniqueClicksTotal) + "'>" + sflpUniqueClicksTotal + "</td>"
				+ "<td style='border-bottom: 2px solid #0e5029; font-weight: bold;" + addNegativeValueColor(flpUniqueClicksTotal) + "'>" + flpUniqueClicksTotal + "</td>"
				+ "<td style='border-bottom: 2px solid #0e5029; font-weight: bold;" + addNegativeValueColor(paidClicksTotal) + "'>" + paidClicksTotal + "</td>"
				+ "<td style='border-bottom: 2px solid #0e5029; font-weight: bold;" + addNegativeValueColor(grossClickDifferenceTotal) + "'>" + grossClickDifferenceTotal + "</td>"
				+ "<td style='border-bottom: 2px solid #0e5029; font-weight: bold;" + addNegativeValueColor(lpGrossClicksTotal) + "'>" + lpGrossClicksTotal + "</td>"
				+ "<td style='border-bottom: 2px solid #0e5029; font-weight: bold;" + addNegativeValueColor(directClickUniqueRevenueTotal) + "'>" + "$" + directClickUniqueRevenueTotal + "</td>"
				+ "<td style='border-bottom: 2px solid #0e5029; font-weight: bold;" + addNegativeValueColor(sflpUniqueClicksRevenueTotal) + "'>" + "$" + sflpUniqueClicksRevenueTotal + "</td>"
				+ "<td style='border-bottom: 2px solid #0e5029; font-weight: bold;" + addNegativeValueColor(flpUniqueClicksRevenueTotal) + "'>" + "$" + flpUniqueClicksRevenueTotal + "</td>"
				+ "<td style='border-bottom: 2px solid #0e5029; font-weight: bold;" + addNegativeValueColor(uniqueClickDifferenceTotal) + "'>" + uniqueClickDifferenceTotal + "</td>"

				+ "<td style='border-bottom: 2px solid #0e5029; font-weight: bold;" + addNegativeValueColor(Float.valueOf(decimal_Format.format(googleAdsRevenueTotal))) + "'>" + "$"
				+ decimal_Format.format(googleAdsRevenueTotal) + "</td>"
				+ "<td style='border-bottom: 2px solid #0e5029; font-weight: bold;" + addNegativeValueColor(Float.valueOf(decimal_Format.format(medianetRevenueTotal))) + "'>"
				+ "$" + decimal_Format.format(medianetRevenueTotal) + "</td>"
				+ "<td style='border-bottom: 2px solid #0e5029; font-weight: bold;" + addNegativeValueColor(Float.valueOf(decimal_Format.format(profitTotal))) + "'>" + "$"
				+ Float.valueOf(decimal_Format.format(profitTotal)) + "</td>"
				+ " </tr>";

		csvStringFirstRow = csvStringFirstRow + ""
				+ "Total" + ","
				+ Float.valueOf(decimal_Format.format(exportSourceCostTotal)) + ","
				+ exportSourceClicksTotal + ","
				+ fbgGrossClicksTotal + ","
				+ fbgUniqueClicksTotal + ","
				+ paidClicksTotal + ","
				+ directClicksTotal + ","
				+ sflpUniqueClicksTotal + ","
				+ flpUniqueClicksTotal + ","
				+ paidClicksTotal + ","
				+ grossClickDifferenceTotal + ","
				+ lpGrossClicksTotal + ","
				+ directClickUniqueRevenueTotal + ","
				+ sflpUniqueClicksRevenueTotal + ","
				+ flpUniqueClicksRevenueTotal + ","
				+ uniqueClickDifferenceTotal + ","
				+ Float.valueOf(decimal_Format.format(googleAdsRevenueTotal)) + ","
				+ Float.valueOf(decimal_Format.format(medianetRevenueTotal)) + ","
				+ profitTotal + "\n";

		csvStringFirstRow = csvStringFirstRow + csvStatsBody;

		body = body + statsBody;

		body = body + "</tbody>"
				+ "</table>"
				+ "</div>";

		emailAndCsvBodyList.add(body);
		emailAndCsvBodyList.add(csvStringFirstRow);

		return emailAndCsvBodyList;
	}

	public String addNegativeValueColor(float value) {

		if (value < 0) {
			return " color: red; font-weight: bold;";
		}

		return "";
	}

}
