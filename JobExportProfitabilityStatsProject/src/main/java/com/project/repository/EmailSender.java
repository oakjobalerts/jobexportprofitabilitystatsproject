package com.project.repository;

import java.util.List;
import java.util.Map;

import com.project.model.EmailReceiver;
import com.project.model.LpClicksModel;
import com.project.model.ProfitabilityModel;

public interface EmailSender {

	public String sendSmtpEmail(String subject, String body, EmailReceiver emailReceiver, String csvData);

	public List<String> createEmailBody(Map<String, ProfitabilityModel> profibalityModelMap, String sourceName, List<String> dateList, Map<String, LpClicksModel> lp_click_map);
}
