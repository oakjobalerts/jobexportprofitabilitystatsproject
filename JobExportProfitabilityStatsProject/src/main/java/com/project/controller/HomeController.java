package com.project.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;

import com.project.model.CpcReveneStatsModel;
import com.project.model.EmailReceiver;
import com.project.model.ExportSourcesModel;
import com.project.model.LpClicksModel;
import com.project.model.ProfitabilityModel;
import com.project.repository.EmailSender;
import com.project.repository.ProfitabilityRepository;
import com.project.utility.DateUtility;

@Controller
public class HomeController {

	@Autowired
	ProfitabilityRepository profitabilityRepository;

	@Autowired
	DateUtility dateUtility;

	@Autowired
	EmailSender emailSender;

	@Autowired
	@Qualifier("emailReceiverObject")
	EmailReceiver emailReceiver;

	public void startProject() {

		String year = dateUtility.getCurrentYear();
		String month = dateUtility.getCurrentMonth();

		List<ExportSourcesModel> exportSourcesModels = profitabilityRepository.getExportSourcesList();

		Map<String, CpcReveneStatsModel> mediaAdsRevenueMap = profitabilityRepository.getMediaAdsRevenueDateWise(year, month);
		Map<String, CpcReveneStatsModel> googleAdsRevenueMap = profitabilityRepository.getGoogleAdsRevenueDateWise(year, month);
		Map<String, LpClicksModel> lp_click_map = profitabilityRepository.getAllLpClicksFromDataBase(year, month);
		List<String> dateList = dateUtility.getAllDatedBetweenTwoDates(dateUtility.getFirstDateOfThisMonth(month), dateUtility.getDateBeofreYesterday());

		Map<String, LpClicksModel> updated_lp_click_map = profitabilityRepository.updateLpclickModel_withGoogle_media_revenue(googleAdsRevenueMap, mediaAdsRevenueMap, lp_click_map, dateList);

		for (ExportSourcesModel exportSourcesModel : exportSourcesModels) {

			if (!exportSourcesModel.isActive()) {
				continue;
			}
			if (exportSourcesModel.getA_id().toLowerCase().equals("viktre")) {
				continue;
			}

			String exportSource = exportSourcesModel.getA_id() + "-jobExport";

			List<CpcReveneStatsModel> landingPageStats = profitabilityRepository.getAllLandingPageClickStatsDateWiseByYearAndMonth(year, month, exportSource);

			List<CpcReveneStatsModel> clicksFromClickogTable = profitabilityRepository.getAllClicksFromClickLogTabes(year, month, exportSource);
			Map<String, CpcReveneStatsModel> actualClicksMap = profitabilityRepository.convertStatsModelClicksIntoDatekeyHashMap(clicksFromClickogTable);

			List<CpcReveneStatsModel> exportProviderStats = profitabilityRepository.getCostAndClicksProvidedByExportSource(year, month, exportSource);
			Map<String, CpcReveneStatsModel> exportProviderMap = profitabilityRepository.convertStatsModelClicksIntoDatekeyHashMap(exportProviderStats);

			List<CpcReveneStatsModel> revenueStatsModelWithLpAndActualClicks = profitabilityRepository
					.convertRevenueModelDateWise(landingPageStats, actualClicksMap, exportProviderMap, dateList);

			List<ProfitabilityModel> profitabilityModels = profitabilityRepository.convertCsvStatsModelIntoProfibalityModel(revenueStatsModelWithLpAndActualClicks);

			Map<String, ProfitabilityModel> profibalityModelMap = profitabilityRepository.convertProfibalityModelIntoDaseWiseHashMap(profitabilityModels);

			List<String> bodyList = emailSender.createEmailBody(profibalityModelMap, exportSourcesModel.getA_id(), dateList, lp_click_map);

			// String csvData =
			// profitabilityRepository.getCsvAsInputStream(profibalityModelMap,
			// exportSourcesModel.getA_id(), dateList);

			emailSender.sendSmtpEmail(exportSource + " profitability Sheet For " + dateUtility.getTodayDate(), bodyList.get(0), emailReceiver, bodyList.get(1));

			// System.out.println(new Gson().toJson(profitabilityModels));
		}
	}
}
