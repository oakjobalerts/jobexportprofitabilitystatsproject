package com.project.utility;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import org.springframework.stereotype.Component;

@Component
public class DateUtility {

    public static DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

    public static DateFormat yearFormatter = new SimpleDateFormat("YYYY");

    public static DateFormat monthFormatter = new SimpleDateFormat("MM");

    public List<String> getDateInterval(String fromDate, String uptoDate) {
        Set<String> dates = new HashSet<String>();
        try {
            TimeZone zone = TimeZone.getTimeZone("EST");
            formatter.setTimeZone(zone);

            Date startDate = (Date) formatter.parse(fromDate);
            Date endDate = (Date) formatter.parse(uptoDate);
            long interval = 24 * 1000 * 60 * 60; // 1 day in millis
            long endTime = endDate.getTime(); // create your endtime here, possibly using Calendar or Date
            long curTime = startDate.getTime();
            while (curTime <= endTime) {
                dates.add(formatter.format(new Date(curTime)));
                curTime += interval;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        List<String> dateList = new ArrayList<String>(dates);
        Collections.sort(dateList);
        return dateList;
    }

    public List<String> getDateIntervalFromMonthYear(String year, String month) {

        List<String> dateList = new ArrayList<String>();

        int intYear = Integer.parseInt(year);
        int intMonth = Integer.parseInt(month);
        int day = 1;

        Calendar startDateCalendar = new GregorianCalendar(intYear, intMonth, day);
        Calendar endDateCalendar = new GregorianCalendar(intYear, intMonth, startDateCalendar.getActualMaximum(Calendar.DAY_OF_MONTH));

        // DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        while (startDateCalendar.getTime().before(endDateCalendar.getTime())) {

            dateList.add(formatter.format(startDateCalendar.getTime()));
            startDateCalendar.add(Calendar.DATE, 1);
        }
        dateList.add(formatter.format(startDateCalendar.getTime()));

        return dateList;
    }

    public int getMaxDaysCountByMonthAndYear(String year, String month) {

        int intYear = Integer.parseInt(year);
        int intMonth = Integer.parseInt(month);
        int day = 1;

        Calendar startDateCalendar = new GregorianCalendar(intYear, intMonth, day);
        return startDateCalendar.getActualMaximum(Calendar.DAY_OF_MONTH);
    }

    public List<String> getAllDatedBetweenTwoDates(String startDate, String endDate) {

        List<String> dateList = new ArrayList<String>();
        dateList.add(startDate);

        try {
            Calendar calendar = Calendar.getInstance();
            Date end = formatter.parse(endDate);
            Date currentDate = formatter.parse(startDate);

            while (currentDate.getTime() < end.getTime()) {

                calendar.setTime(currentDate);
                calendar.add(Calendar.DATE, 1);
                currentDate = calendar.getTime();
                dateList.add(new String(formatter.format(currentDate)));
            }
            return dateList;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public String getFirstDateOfThisMonth(String month) {

        Integer intMonth = Integer.parseInt(month);

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MONTH, intMonth - 1);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        return formatter.format(calendar.getTime());

    }

    public String getCurrentYear() {

        return yearFormatter.format(new Date());
    }

    public String getCurrentMonth() {

        return monthFormatter.format(new Date());
    }

    public String getTodayDate() {

        return formatter.format(new Date());
    }
    
    public String getDateBeofreYesterday() {

        return formatter.format(yesterday());
    }
    
    private Date yesterday() {
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -2);
        return cal.getTime();
    }

}
