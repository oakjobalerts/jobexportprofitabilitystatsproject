package com.project.application;

import javax.persistence.EntityManagerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.vendor.HibernateJpaSessionFactoryBean;

import com.project.controller.HomeController;
import com.project.repository.ProfitabilityRepository;

@SpringBootApplication
@ComponentScan(basePackages = "com.project.*")
@PropertySource("classpath:/smtp.properties")
@EnableJpaRepositories(basePackageClasses = ProfitabilityRepository.class)
// @EntityScan(basePackageClasses = JobExportCostModel.class)
public class Application implements CommandLineRunner {

    @Autowired
    HomeController homeController;
//    

    public static void main(String[] args) {

        SpringApplication.run(Application.class, args);

    }

    @Bean
    public HibernateJpaSessionFactoryBean sessionFactory(EntityManagerFactory emf) {
        HibernateJpaSessionFactoryBean factory = new HibernateJpaSessionFactoryBean();
        factory.setEntityManagerFactory(emf);
        return factory;
    }

    @Override
    public void run(String... args) throws Exception {

        System.out.println("starting project");
        homeController.startProject();

    }

}
