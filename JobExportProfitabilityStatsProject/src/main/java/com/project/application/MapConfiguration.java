package com.project.application;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.google.gson.GsonBuilder;
import com.project.model.EmailReceiver;

@Configuration
public class MapConfiguration {

    @Value("${email.receiver.file}")
    String emailReceiverJsonFile;

    @Bean(name = "emailReceiverObject")
    public EmailReceiver getEmailReceivers() {

        try {
            Resource resource = new ClassPathResource(emailReceiverJsonFile);
            InputStream resourceInputStream = resource.getInputStream();

            BufferedReader reader = new BufferedReader(new InputStreamReader(resourceInputStream));

            EmailReceiver emailReceiver = new GsonBuilder().setPrettyPrinting().create().fromJson(reader, EmailReceiver.class);
            return emailReceiver;

        } catch (Exception e) {
            e.printStackTrace();
            return new EmailReceiver();
        }
    }

}
