package com.project.model;

public class ProfitabilityModel {

	String date;
	Number exportCost;
	Number exportClicks;

	// exportClicks - grossClicks
	Number grossClickDiference;

	// grossClickDiference / exportClicks
	Number grossClickDiffPer;

	Number uniqueClicksOak;
	Number grossClicksOak;

	Number lpUniqueClicks;
	Number flpUniqueClicks;
	Number sflpUniqueClicks;
	Number lpGrossClicks;
	Number flpGrossClicks;
	Number sflpGrossClicks;

	Number flpUniqueRevenue;
	Number sflpUniqueRevenue;
	Number lpUniqueRevenue;

	Number flpGrossRevenue;
	Number sflpGrossRevenue;
	Number lpGrossRevenue;

	Number uniqueRevenue;
	Number grossRevenue;

	// uniqueClicksOak - exportClicks
	Number clickDifference;

	// uniqueRevenue - exportCost
	Number profit;


	public Number getFlpUniqueRevenue() {
		return flpUniqueRevenue;
	}

	public Number getSflpUniqueRevenue() {
		return sflpUniqueRevenue;
	}

	public Number getFlpGrossRevenue() {
		return flpGrossRevenue;
	}

	public Number getLpUniqueRevenue() {
		return lpUniqueRevenue;
	}

	public Number getLpGrossRevenue() {
		return lpGrossRevenue;
	}

	public void setLpUniqueRevenue(Number lpUniqueRevenue) {
		this.lpUniqueRevenue = lpUniqueRevenue;
	}

	public void setLpGrossRevenue(Number lpGrossRevenue) {
		this.lpGrossRevenue = lpGrossRevenue;
	}

	public Number getSflpGrossRevenue() {
		return sflpGrossRevenue;
	}

	public void setFlpUniqueRevenue(Number flpUniqueRevenue) {
		this.flpUniqueRevenue = flpUniqueRevenue;
	}

	public void setSflpUniqueRevenue(Number sflpUniqueRevenue) {
		this.sflpUniqueRevenue = sflpUniqueRevenue;
	}

	public void setFlpGrossRevenue(Number flpGrossRevenue) {
		this.flpGrossRevenue = flpGrossRevenue;
	}

	public void setSflpGrossRevenue(Number sflpGrossRevenue) {
		this.sflpGrossRevenue = sflpGrossRevenue;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Number getExportCost() {
		return exportCost;
	}

	public void setExportCost(Number exportCost) {
		this.exportCost = exportCost;
	}

	public Number getExportClicks() {
		return exportClicks;
	}

	public void setExportClicks(Number exportClicks) {
		this.exportClicks = exportClicks;
	}

	public Number getGrossClickDiference() {
		return grossClickDiference;
	}

	public void setGrossClickDiference(Number grossClickDiference) {
		this.grossClickDiference = grossClickDiference;
	}

	public Number getGrossClickDiffPer() {
		return grossClickDiffPer;
	}

	public void setGrossClickDiffPer(Number grossClickDiffPer) {
		this.grossClickDiffPer = grossClickDiffPer;
	}

	public Number getUniqueClicksOak() {
		return uniqueClicksOak;
	}

	public void setUniqueClicksOak(Number uniqueClicksOak) {
		this.uniqueClicksOak = uniqueClicksOak;
	}

	public Number getGrossClicksOak() {
		return grossClicksOak;
	}

	public void setGrossClicksOak(Number grossClicksOak) {
		this.grossClicksOak = grossClicksOak;
	}

	public Number getLpUniqueClicks() {
		return lpUniqueClicks;
	}

	public Number getFlpUniqueClicks() {
		return flpUniqueClicks;
	}

	public Number getSflpUniqueClicks() {
		return sflpUniqueClicks;
	}

	public Number getLpGrossClicks() {
		return lpGrossClicks;
	}

	public Number getFlpGrossClicks() {
		return flpGrossClicks;
	}

	public Number getSflpGrossClicks() {
		return sflpGrossClicks;
	}

	public void setLpUniqueClicks(Number lpUniqueClicks) {
		this.lpUniqueClicks = lpUniqueClicks;
	}

	public void setFlpUniqueClicks(Number flpUniqueClicks) {
		this.flpUniqueClicks = flpUniqueClicks;
	}

	public void setSflpUniqueClicks(Number sflpUniqueClicks) {
		this.sflpUniqueClicks = sflpUniqueClicks;
	}

	public void setLpGrossClicks(Number lpGrossClicks) {
		this.lpGrossClicks = lpGrossClicks;
	}

	public void setFlpGrossClicks(Number flpGrossClicks) {
		this.flpGrossClicks = flpGrossClicks;
	}

	public void setSflpGrossClicks(Number sflpGrossClicks) {
		this.sflpGrossClicks = sflpGrossClicks;
	}

	public Number getUniqueRevenue() {
		return uniqueRevenue;
	}

	public void setUniqueRevenue(Number uniqueRevenue) {
		this.uniqueRevenue = uniqueRevenue;
	}

	public Number getGrossRevenue() {
		return grossRevenue;
	}

	public void setGrossRevenue(Number grossRevenue) {
		this.grossRevenue = grossRevenue;
	}

	public Number getClickDifference() {
		return clickDifference;
	}

	public void setClickDifference(Number clickDifference) {
		this.clickDifference = clickDifference;
	}

	public Number getProfit() {
		return profit;
	}

	public void setProfit(Number profit) {
		this.profit = profit;
	}

}
