package com.project.model;

import java.util.Date;

public class LpClicksModel {

	public Date dbDate;
	public Number lp_count;
	// total goole revenue divided by total lp click count
	public float googleUpdatedAdsRevenue;
	// total media revenue divided by total lp click count
	public float mediaUpdatedAdsRevenue;

	public float getGoogleUpdatedAdsRevenue() {
		return googleUpdatedAdsRevenue;
	}

	public void setGoogleUpdatedAdsRevenue(float googleUpdatedAdsRevenue) {
		this.googleUpdatedAdsRevenue = googleUpdatedAdsRevenue;
	}

	public float getMediaUpdatedAdsRevenue() {
		return mediaUpdatedAdsRevenue;
	}

	public void setMediaUpdatedAdsRevenue(float mediaUpdatedAdsRevenue) {
		this.mediaUpdatedAdsRevenue = mediaUpdatedAdsRevenue;
	}

	public Date getDbDate() {
		return dbDate;
	}

	public void setDbDate(Date dbDate) {
		this.dbDate = dbDate;
	}

	public Number getLp_count() {
		return lp_count;
	}

	public void setLp_count(Number lp_count) {
		this.lp_count = lp_count;
	}

}
