package com.project.model;

import java.util.Date;

public class CpcReveneStatsModel {

	String botClicks;
	Date dbDate;

	Number lpUniqueClicks;
	Number flpUniqueClicks;
	Number sflpUniqueClicks;

	Number lpGrossClicks;
	Number flpGrossClicks;
	Number sflpGrossClicks;

	Number uniqueRevenueFromDb;
	Number grossRevenueFromDb;

	Number lpUniqueRevenue;
	Number lpGrossRevenue;

	Number sflpUniqueRevenue;
	Number sflpGrossRevenue;

	Number flpUniqueRevenue;
	Number flpGrossRevenue;

	String lpClickType;
	Number dbUniqueClickCount;
	Number dbGrossClickCount;

	Number uniqueCicks;
	Number grossClicks;
	Number uniqueRevenue;
	Number grossRevenue;

	Number exportSourceClicks;
	Number exportSourceCost;

	String date;

	Number googleAdsRevenue;
	Number mediaAdsRevenue;

	public Number getGoogleAdsRevenue() {
		return googleAdsRevenue;
	}

	public void setGoogleAdsRevenue(Number googleAdsRevenue) {
		this.googleAdsRevenue = googleAdsRevenue;
	}

	public Number getMediaAdsRevenue() {
		return mediaAdsRevenue;
	}

	public void setMediaAdsRevenue(Number mediaAdsRevenue) {
		this.mediaAdsRevenue = mediaAdsRevenue;
	}

	public Number getSflpUniqueRevenue() {
		return sflpUniqueRevenue;
	}

	public Number getSflpGrossRevenue() {
		return sflpGrossRevenue;
	}

	public Number getFlpUniqueRevenue() {
		return flpUniqueRevenue;
	}

	public Number getFlpGrossRevenue() {
		return flpGrossRevenue;
	}

	public void setSflpUniqueRevenue(Number sflpUniqueRevenue) {
		this.sflpUniqueRevenue = sflpUniqueRevenue;
	}

	public void setSflpGrossRevenue(Number sflpGrossRevenue) {
		this.sflpGrossRevenue = sflpGrossRevenue;
	}

	public void setFlpUniqueRevenue(Number flpUniqueRevenue) {
		this.flpUniqueRevenue = flpUniqueRevenue;
	}

	public void setFlpGrossRevenue(Number flpGrossRevenue) {
		this.flpGrossRevenue = flpGrossRevenue;
	}

	public Number getExportSourceClicks() {
		return exportSourceClicks;
	}

	public void setExportSourceClicks(Number exportSourceClicks) {
		this.exportSourceClicks = exportSourceClicks;
	}

	public Number getExportSourceCost() {
		return exportSourceCost;
	}

	public void setExportSourceCost(Number exportSourceCost) {
		this.exportSourceCost = exportSourceCost;
	}

	public Number getUniqueCicks() {
		return uniqueCicks;
	}

	public void setUniqueCicks(Number uniqueCicks) {
		this.uniqueCicks = uniqueCicks;
	}

	public Number getGrossClicks() {
		return grossClicks;
	}

	public void setGrossClicks(Number grossClicks) {
		this.grossClicks = grossClicks;
	}

	public Number getUniqueRevenue() {
		return uniqueRevenue;
	}

	public void setUniqueRevenue(Number uniqueRevenue) {
		this.uniqueRevenue = uniqueRevenue;
	}

	public Number getGrossRevenue() {
		return grossRevenue;
	}

	public void setGrossRevenue(Number grossRevenue) {
		this.grossRevenue = grossRevenue;
	}

	public String getBotClicks() {
		return botClicks;
	}

	public void setBotClicks(String botClicks) {
		this.botClicks = botClicks;
	}

	public Date getDbDate() {
		return dbDate;
	}

	public void setDbDate(Date dbDate) {
		this.dbDate = dbDate;
	}

	public Number getLpUniqueClicks() {
		return lpUniqueClicks;
	}

	public Number getFlpUniqueClicks() {
		return flpUniqueClicks;
	}

	public Number getSflpUniqueClicks() {
		return sflpUniqueClicks;
	}

	public Number getLpGrossClicks() {
		return lpGrossClicks;
	}

	public Number getFlpGrossClicks() {
		return flpGrossClicks;
	}

	public Number getSflpGrossClicks() {
		return sflpGrossClicks;
	}

	public void setLpUniqueClicks(Number lpUniqueClicks) {
		this.lpUniqueClicks = lpUniqueClicks;
	}

	public void setFlpUniqueClicks(Number flpUniqueClicks) {
		this.flpUniqueClicks = flpUniqueClicks;
	}

	public void setSflpUniqueClicks(Number sflpUniqueClicks) {
		this.sflpUniqueClicks = sflpUniqueClicks;
	}

	public void setLpGrossClicks(Number lpGrossClicks) {
		this.lpGrossClicks = lpGrossClicks;
	}

	public void setFlpGrossClicks(Number flpGrossClicks) {
		this.flpGrossClicks = flpGrossClicks;
	}

	public void setSflpGrossClicks(Number sflpGrossClicks) {
		this.sflpGrossClicks = sflpGrossClicks;
	}

	public Number getLpUniqueRevenue() {
		return lpUniqueRevenue;
	}

	public void setLpUniqueRevenue(Number lpUniqueRevenue) {
		this.lpUniqueRevenue = lpUniqueRevenue;
	}

	public Number getLpGrossRevenue() {
		return lpGrossRevenue;
	}

	public void setLpGrossRevenue(Number lpGrossRevenue) {
		this.lpGrossRevenue = lpGrossRevenue;
	}

	public String getLpClickType() {
		return lpClickType;
	}

	public void setLpClickType(String lpClickType) {
		this.lpClickType = lpClickType;
	}

	public Number getUniqueRevenueFromDb() {
		return uniqueRevenueFromDb;
	}

	public Number getGrossRevenueFromDb() {
		return grossRevenueFromDb;
	}

	public Number getDbUniqueClickCount() {
		return dbUniqueClickCount;
	}

	public Number getDbGrossClickCount() {
		return dbGrossClickCount;
	}

	public void setUniqueRevenueFromDb(Number uniqueRevenueFromDb) {
		this.uniqueRevenueFromDb = uniqueRevenueFromDb;
	}

	public void setGrossRevenueFromDb(Number grossRevenueFromDb) {
		this.grossRevenueFromDb = grossRevenueFromDb;
	}

	public void setDbUniqueClickCount(Number dbUniqueClickCount) {
		this.dbUniqueClickCount = dbUniqueClickCount;
	}

	public void setDbGrossClickCount(Number dbGrossClickCount) {
		this.dbGrossClickCount = dbGrossClickCount;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

}
